<?php
/**
 * @file
 * Various help functions.
 */

/**
 * Form builder for bnm_rates_settings_form form.
 *
 * @see system_settings_form()
 */
function bnm_rates_settings_form($form_state) {
  $lang = bnm_rates_current_lang();

  $currencies = bnm_rates_currency_list($lang);

  $form = array();
  $form['currency_items']['#tree'] = TRUE;

  foreach ($currencies as $currency) {
    $form['currency_items'][$currency->valute_id] = array(
      'currency_name' => array(
        '#markup' => check_plain($currency->currency_name),
      ),
      'char_code' => array(
        '#markup' => check_plain($currency->char_code),
      ),
      'in_block' => array(
        '#type' => 'checkbox',
        '#default_value' => $currency->in_block,
      ),
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $currency->weight,
        '#delta' => 50,
        '#title_display' => 'invisible',
      ),
    );
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));
  return $form;
}

/**
 * Display settings form.
 *
 * @param array $variables
 *   An associative array with an 'form' element.
 *
 * @return string
 *   Returns html.
 */
function theme_bnm_rates_settings_form($variables = array()) {
  $form = $variables['form'];
  $rows = array();

  foreach (element_children($form['currency_items']) as $valute_id) {
    $form['currency_items'][$valute_id]['weight']['#attributes']['class'] = array('currency-item-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($form['currency_items'][$valute_id]['currency_name']),
        drupal_render($form['currency_items'][$valute_id]['char_code']),
        drupal_render($form['currency_items'][$valute_id]['in_block']),
        drupal_render($form['currency_items'][$valute_id]['weight']),
      ),
      'class' => array('draggable'),
    );

  }

  $header = array(
    t('Currency Name'),
    t('Code'),
    t('Show in block'),
    t('Weight'),
  );

  $table_id = 'currency-items-table';

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  $output .= drupal_render_children($form);

  // We now call the drupal_add_tabledrag() function in order to add the
  // tabledrag.js goodness onto our page.
  //
  // For a basic sortable table, we need to pass it:
  // - the $table_id of our <table> element,
  // - the $action to be performed on our form items ('order'),
  // - a string describing where $action should be applied ('siblings'),
  // - and the class of the element containing our 'weight' element.
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'currency-item-weight');

  return $output;
}


/**
 * Implements hook_form_submit().
 */
function bnm_rates_settings_form_submit($form, &$form_state) {
  foreach ($form_state['values']['currency_items'] as $id => $item) {
    db_update('bnm_currency')
      ->fields(array(
        'weight' => $item['weight'],
        'in_block' => $item['in_block'],
      ))
      ->condition('valute_id', $id)
      ->execute();
  }
}

/**
 * Select currency list from database.
 *
 * @param string $lang
 *   Language.
 *
 * @return mixed
 *   List of currencies in current language.
 */
function bnm_rates_currency_list($lang = 'en') {
  $query = "SELECT valute_id, char_code, currency_name, lang, in_block, weight
            FROM {bnm_currency}
            WHERE lang = :lang
            ORDER BY weight";
  $result = db_query($query, array(':lang' => $lang))->fetchAll();

  return $result;
}
