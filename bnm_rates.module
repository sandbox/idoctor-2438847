<?php
/**
 * @file
 * This module retrieves currency rates from National Bank of Moldova.
 */

/**
 * Implements hook_menu().
 */
function bnm_rates_menu() {
  $items['admin/config/bnm_rates'] = array(
    'title' => 'BNM rates settings',
    'description' => 'Show a page with a sortable tabledrag form',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('bnm_rates_settings_form'),
    'access callback' => TRUE,
    'file' => 'include/bnm_rates_settings_form.inc.php',
  );

  return $items;
}

/**
 * Implements hook_help().
 */
function bnm_rates_help($path, $arg) {
  if ($path == 'admin/help#bnm_rates') {
    return t('Module bnm_rates pulls <a href="@xml">xml</a> from
              <a href="@bnm">www.bnm.org</a> and displays exchange rates.',
      array(
        '@xml' => 'http://www.bnm.org/en/' .
        'official_exchange_rates?get_xml=1&date=' . date('d.m.Y'),
        '@bnm' => 'http://www.bnm.org',
      ));
  }
}

/**
 * Implements hook_block_info().
 */
function bnm_rates_block_info() {
  $blocks['block_bnm_rates'] = array(
    'info' => t('BNM currency rates'),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function bnm_rates_block_view($delta = '') {
  // This example is adapted from node.module.
  $block = array();

  $date = date("d.m.Y");
  $lang = bnm_rates_current_lang();

  switch ($delta) {
    case 'block_bnm_rates':
      $block_subject = variable_get('bnm_rates_block_title_' . $lang,
                          t('BNM Official exchange rate @date', array('@date' => $date)));
      $block['subject'] = $block_subject;
      module_load_include('php', 'bnm_rates', 'include/bnm_rates_functions.inc');
      if ($rates = bnm_rates_get($date, $lang, 1)) {
        $block['content'] = array(
          '#theme' => 'bnm_rates_block_rates',
          '#title' => $block_subject,
          '#rates' => $rates,
        );
      }
      else {
        $block['content'] = t('No content available.');
      }
      break;
  }
  return $block;
}


/**
 * Implements hook_theme().
 */
function bnm_rates_theme($existing, $type, $theme, $path) {
  $bnm_rates_themes = array(
    'bnm_rates_block_rates' => array(
      'variables' => array('rates' => NULL, 'limit' => 10),
      'template' => 'templates/bnm_rates',
    ),
    'bnm_rates_settings_form' => array(
      'render element' => 'form',
      'file' => 'templates/bnm_rates_settings.tpl.php',
    ),
  );

  return $bnm_rates_themes;
}

/**
 * Sets the language.
 *
 * @return string $lang
 *   User's current language.
 */
function bnm_rates_current_lang() {
  global $language;
  $lang = $language->language;
  if (in_array($lang, array('ro', 'mo'))) {
    $lang = 'md';
  }

  return $lang;
}
